import { Provider } from "react-redux";
import store from "./Redux/store";
import StopWatch from "./components/StopWatch";

function App() {
  return (
    <Provider store={store}>
      <StopWatch />
    </Provider>
  );
}

export default App;
