import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useSelector } from "react-redux";
import FormatTimer from "./FormatTimer";

function Laps() {
  const laps = useSelector((state) => state.time.laps);
  return (
    <View style={styles.laps__container}>
      {laps?.length !== 0 && <Text>LAPS</Text>}

      {!laps?.length !== 0 &&
        laps.map((lap, idx) => (
          <View key={idx} style={styles.laps}>
            <Text style={{ color: "white" }}>Lap {idx + 1} :</Text>
            <Text style={{ color: "white" }}>
              <FormatTimer timer={lap} />
            </Text>
          </View>
        ))}
    </View>
  );
}

export default Laps;
const styles = StyleSheet.create({
  laps__container: {
    width: "100%",
  },
  laps: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
});
