import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  lapTimer,
  pauseTimer,
  resetTimer,
  resumeTimer,
  startTimer,
} from "../Redux/Actions/Actions";
import { Button, View, Text, StyleSheet, TouchableOpacity } from "react-native";

function Buttons() {
  const isClicked = useSelector((state) => state.time.isClicked);
  const start = useSelector((state) => state.time.start);
  const dispatch = useDispatch();

  const handleTimer = () => {
    if (start) {
      dispatch(pauseTimer());
    } else {
      dispatch(resumeTimer());
    }
  };
  return (
    <View style={styles.btns__container}>
      {!isClicked ? (
        <TouchableOpacity style={{ width: 100, marginTop: 20 }}>
          <Button title="Start" onPress={() => dispatch(startTimer())} />
        </TouchableOpacity>
      ) : (
        <View style={styles.pauseResumeBtn}>
          <TouchableOpacity style={{ width: 100 }}>
            <Button
              color="coral"
              title={`${start ? "pause" : "resume"}`}
              onPress={handleTimer}
            />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: 20, width: 100 }}>
            <Button title="laps" onPress={() => dispatch(lapTimer())} />
          </TouchableOpacity>
          <TouchableOpacity style={{ width: 100 }}>
            <Button title="reset" onPress={() => dispatch(resetTimer())} />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
}

export default Buttons;
const styles = StyleSheet.create({
  btns__container: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  pauseResumeBtn: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
});
