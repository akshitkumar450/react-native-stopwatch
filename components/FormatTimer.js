import React from "react";
import { StyleSheet, Text, View } from "react-native";

function FormatTimer({ timer }) {
  return (
    <View style={styles.formatTimer__container}>
      {/**Showing the current timer */}
      <Text style={styles.text}>
        {`0${Math.floor(timer / 3600)}`.slice(-2)}:
      </Text>
      <Text style={styles.text}>{`0${Math.floor(timer / 60)}`.slice(-2)}:</Text>
      <Text style={styles.text}>{`0${timer % 60}`.slice(-2)}</Text>
    </View>
  );
}

export default FormatTimer;
const styles = StyleSheet.create({
  formatTimer__container: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  text: {
    color: "white",
    fontSize: 20,
  },
});
