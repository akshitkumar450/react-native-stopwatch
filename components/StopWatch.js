import { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { secondsTimer } from "../Redux/Actions/Actions";
import Buttons from "./Buttons";
import FormatTimer from "./FormatTimer";
import Laps from "./Laps";

const StopWatch = () => {
  const timer = useSelector((state) => state.time.timer);
  const start = useSelector((state) => state.time.start);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("useEffect running");
    let interval;
    if (start === true) {
      interval = setInterval(() => {
        dispatch(secondsTimer());
      }, 10);
    }

    // clean up function
    return () => {
      clearInterval(interval);
    };
  }, [dispatch, start]);

  return (
    <View style={styles.container}>
      <Text style={{ color: "white", textAlign: "center", fontSize: 25 }}>
        STOPWATCH
      </Text>
      <FormatTimer timer={timer} />
      <Buttons />
      <Laps />
    </View>
  );
};

export default StopWatch;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    paddingTop: 50,
  },
});
